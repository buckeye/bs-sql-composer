module StdLib = {
  module List = Belt.List;

  module Option = Belt.Option;

  module String = String;
};

include SqlComposer_query.Make(StdLib);
