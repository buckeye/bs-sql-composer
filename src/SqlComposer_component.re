module type Type = {
  module Assignment: {
    type t;
    let add: (option(t), string, string) => option(t);
    let render: option(t) => option(string);
  };

  module Fields: {
    type t;
    let add: (option(t), string) => option(t);
    let render: option(t) => option(string);
  };

  module From: {
    type t;
    let make: string => t;
    let render: option(t) => option(string);
    let raw: option(t) => option(string);
  };

  module GroupBy: {
    type t;
    let add: (option(t), string) => option(t);
    let render: option(t) => option(string);
  };

  module Join: {
    type t;
    let add: (option(t), string) => option(t);
    let render: option(t) => option(string);
  };

  module Limit: {
    type t;
    let withCount: int => t;
    let withOffset: (int, int) => t;
    let render: option(t) => option(string);
  };

  module type FlagType = {
    type flag;
    let toString: flag => string;
  };

  module Modifier:
    (Flag: FlagType) =>
     {
      type t;
      let add: (option(t), Flag.flag) => option(t);
      let render: option(t) => option(string);
    };

  module OrderBy: {
    type t;
    let add: (option(t), [ | `Asc(string) | `Desc(string)]) => option(t);
    let render: option(t) => option(string);
  };

  module Table: {
    type t;
    let make: string => t;
    let render: option(t) => option(string);
  };

  module Where: {
    type t;
    let add: (option(t), string) => option(t);
    let render: option(t) => option(string);
  };
};

module Make = (Std: SqlComposer_stdlib.Type) : Type => {
  module type ClauseType = {
    let prefix: string;

    let separator: string;
  };

  let addToMaybeList = (maybeList, thing) =>
    switch (maybeList) {
    | Some(list) => Some(Std.List.add(list, thing))
    | None => Some(Std.List.make(1, thing))
    };

  module Clause = (Type: ClauseType) => {
    type t = Std.List.t(string);

    let add = addToMaybeList;

    let render = maybe =>
      Std.Option.map(maybe, t =>
        Std.List.reverse(t)
        |> Std.String.concat(Type.separator)
        |> (x => Type.prefix ++ x)
      );
  };

  module Assignment = {
    type assignment = (string, string);

    type t = Std.List.t(assignment);

    let add = (t, field, value) => addToMaybeList(t, (field, value));

    let render = maybe =>
      Std.Option.map(maybe, t =>
        Std.List.reverse(t)
        ->(Std.List.map(((field, value)) => {j|$field = $value|j}))
        ->(x => Std.String.concat("\n, ", x)->(x => "SET\n  " ++ x))
      );
  };

  module Fields =
    Clause({
      let prefix = "  ";
      let separator = "\n, ";
    });

  module From = {
    type t = string;

    let make = (string): t => string;

    let render = maybe => Std.Option.map(maybe, t => {j|FROM $t|j});

    let raw = maybe => Std.Option.map(maybe, t => {j|  $t|j});
  };

  module GroupBy =
    Clause({
      let prefix = "GROUP BY\n  ";
      let separator = "\n, ";
    });

  module Join =
    Clause({
      let prefix = "";
      let separator = "\n";
    });

  module Limit = {
    type t =
      | Count(int)
      | Offset(int, int);

    let withCount = count => Count(count);

    let withOffset = (count, offset) => Offset(count, offset);

    let render = maybe =>
      Std.Option.map(
        maybe,
        fun
        | Count(count) => {j|LIMIT $count|j}
        | Offset(count, offset) => {j|LIMIT $count OFFSET $offset|j},
      );
  };

  module type FlagType = {
    type flag;
    let toString: flag => string;
  };

  module Modifier = (Flag: FlagType) => {
    type t = Std.List.t(Flag.flag);

    let add = addToMaybeList;

    let render = maybe =>
      Std.Option.map(maybe, t =>
        Std.List.map(t, Flag.toString)
        |> Std.List.reverse
        |> Std.String.concat(" ")
        |> (x => " " ++ x)
      );
  };

  module OrderBy = {
    type ord = [ | `Asc(string) | `Desc(string)];
    type t = Std.List.t(ord);

    let add = addToMaybeList;

    let toString =
      fun
      | `Asc(string) => {j|$string ASC|j}
      | `Desc(string) => {j|$string DESC|j};

    let render = maybe =>
      Std.Option.map(maybe, t =>
        Std.List.map(t, toString)
        |> Std.List.reverse
        |> Std.String.concat("\n, ")
        |> (x => "ORDER BY\n  " ++ x)
      );
  };

  module Table = {
    type t = string;

    let make = (string): t => string;

    let render = maybe => Std.Option.map(maybe, t => {j|  $t|j});
  };

  module Where =
    Clause({
      let prefix = "WHERE 1=1\n";
      let separator = "\n";
    });
};

include Make({
  module List = Belt.List;

  module Option = Belt.Option;

  module String = String;
});
